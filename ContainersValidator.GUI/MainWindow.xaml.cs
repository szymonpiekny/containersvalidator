﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using ContainersValidator.BLL.CheckDigitCalculators;
using ContainersValidator.BLL.Services;
using ContainersValidator.BLL.Validators;
using ContainersValidator.GUI.ViewModel;
using ContainersValidator.GUI.ViewServices;
using ContainersValidator.Models;
using FluentValidation;
using System.Windows;

namespace ContainersValidator.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var container = new WindsorContainer();
            container.Register(Component.For<IOutput, FileOpenService>());
            container.Register(Component.For<ICheckDigitCalculator, CheckDigitCalculator>());
            container.Register(Component.For<AbstractValidator<Container>, ContainerValidator>());
            container.Register(Component.For<IContainerFileService, ContainerFileService>());

            var mainWindowViewModel = new MainWindowViewModel(container.Resolve<IOutput>(), container.Resolve<IContainerFileService>());
            DataContext = mainWindowViewModel;
        }
    }
}