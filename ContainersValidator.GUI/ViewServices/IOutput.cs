﻿namespace ContainersValidator.GUI.ViewServices
{
    public interface IOutput
    {
        string OpenFileDialog();
    }
}