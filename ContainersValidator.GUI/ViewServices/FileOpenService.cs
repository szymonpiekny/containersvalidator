﻿using Microsoft.Win32;
using System;

namespace ContainersValidator.GUI.ViewServices
{
    public class FileOpenService : IOutput
    {
        public string OpenFileDialog()
        {
            var selectedFilePath = string.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "Text files (*.txt)|*.txt";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (openFileDialog.ShowDialog() == true)
            {
                selectedFilePath = openFileDialog.FileName;
            }

            return selectedFilePath;
        }
    }
}