﻿using System;
using System.Windows;
using System.Windows.Data;

namespace ContainersValidator.GUI.ViewServices.Converters
{
    public class BooleanToHiddenVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility rv = Visibility.Visible;
            try
            {
                var x = bool.Parse(value.ToString());
                if (parameter != null && parameter.Equals("invBool"))
                {
                    x = !x;
                }

                if (x)
                {
                    rv = Visibility.Visible;
                }
                else
                {
                    rv = Visibility.Hidden;
                }
            }
            catch (Exception)
            {
            }
            return rv;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}