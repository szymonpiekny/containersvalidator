﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace ContainersValidator.GUI.ViewServices.Converters
{
    public class ListToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Join($", {Environment.NewLine}", (IEnumerable<string>)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((string)value).Split(',');
        }
    }
}