﻿using Serilog;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;

namespace ContainersValidator.GUI.ViewModel
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        protected void RaisePropertyChanged([CallerMemberName] String propertyName = "")
        {
            try
            {
                if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(propertyName)); }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wystąpił błąd", ex.Message);
                Log.Error(ex.Message, ex);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private bool? _CloseWindowFlag;

        public bool? CloseWindowFlag
        {
            get { return _CloseWindowFlag; }
            set
            {
                _CloseWindowFlag = value;
                RaisePropertyChanged("CloseWindowFlag");
            }
        }

        public virtual void CloseWindow(bool? result = true)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    CloseWindowFlag = CloseWindowFlag == null
                        ? true
                        : !CloseWindowFlag;
                }));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wystąpił błąd", ex.Message);
                Log.Error(ex.Message, ex);
            }
        }
    }
}