﻿using ContainersValidator.BLL.Services;
using ContainersValidator.GUI.ViewServices;
using ContainersValidator.Models;
using MvvmFoundation.Wpf;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ContainersValidator.GUI.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {
        private IOutput _openFileService;
        private IContainerFileService _containerFileService;
        private string _selectedFile;
        private bool _canValidateFile;
        private string _validationProgressStatus;
        private List<ContainerValidationResult> _validationResults { get; set; }

        public MainWindowViewModel(IOutput openFileService, IContainerFileService containerFileService)
        {
            _canValidateFile = true;
            _containerFileService = containerFileService;
            _openFileService = openFileService;

            CreateCommands();
        }

        public ICommand SelectFileCommand { get; internal set; }
        public ICommand ValidateFileCommand { get; internal set; }
        public ICommand CloseApplicationCommand { get; internal set; }

        private void CreateCommands()
        {
            CloseApplicationCommand = new RelayCommand(CloseApplication);
            SelectFileCommand = new RelayCommand(SelectFile);
            ValidateFileCommand = new RelayCommand(ValidateFileAsync);
        }

        private void SelectFile()
        {
            try
            {
                SelectedFile = _openFileService.OpenFileDialog();
            }
            catch (Exception ex)
            {
                _validationProgressStatus = "Couldn't open file please check logs for more details";
                RaisePropertyChanged(nameof(ValidationProgressStatus));
                Log.Error(ex, "Couldn't open file");
            }
        }

        private async void ValidateFileAsync()
        {
            try
            {
                _canValidateFile = false;
                RaisePropertyChanged(nameof(CanValidateFile));

                _validationProgressStatus = "Validating...";
                RaisePropertyChanged(nameof(ValidationProgressStatus));
                var fileLines = await Task.Run(() => _containerFileService.ParseFile(SelectedFile));
                _validationResults = await Task.Run(() => _containerFileService.Validate(fileLines));
                RaisePropertyChanged(nameof(ValidationResults));

                _canValidateFile = true;
                RaisePropertyChanged(nameof(CanValidateFile));

                _validationProgressStatus = "Validated!";
                RaisePropertyChanged(nameof(ValidationProgressStatus));
            }
            catch (Exception ex)
            {
                _validationProgressStatus = "Couldn't validate file please check logs for more details";
                RaisePropertyChanged(nameof(ValidationProgressStatus));
                Log.Error(ex, "Couldn't validate file");
            }
        }

        private void CloseApplication()
        {
            Application.Current.Shutdown();
        }

        public string SelectedFile
        {
            get
            {
                return _selectedFile;
            }

            set
            {
                if (value != _selectedFile)
                {
                    _selectedFile = value;
                    RaisePropertyChanged();
                    RaisePropertyChanged(nameof(CanValidateFile));
                }
            }
        }

        public bool CanValidateFile
        {
            get
            {
                return !string.IsNullOrWhiteSpace(SelectedFile) && _canValidateFile;
            }

            set
            {
                _canValidateFile = value;
                RaisePropertyChanged();
            }
        }

        public string ValidationProgressStatus
        {
            get
            {
                return _validationProgressStatus;
            }

            set
            {
                if (value != _validationProgressStatus)
                {
                    _validationProgressStatus = value;
                    RaisePropertyChanged();
                }
            }
        }

        public List<ContainerValidationResult> ValidationResults
        {
            get
            {
                return _validationResults;
            }
        }
    }
}