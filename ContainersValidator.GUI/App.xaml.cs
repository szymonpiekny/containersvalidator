﻿using Castle.Windsor;
using Castle.Windsor.Installer;
using Serilog;
using System.Windows;

namespace ContainersValidator.GUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .WriteTo.File("logs/logs.log", rollingInterval: RollingInterval.Day)
               .CreateLogger();
        }
    }
}