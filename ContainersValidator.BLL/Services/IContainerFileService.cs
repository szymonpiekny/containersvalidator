﻿using ContainersValidator.Models;
using System.Collections.Generic;

namespace ContainersValidator.BLL.Services
{
    public interface IContainerFileService
    {
        List<ContainerValidationResult> Validate(List<Container> containers);
        List<Container> ParseFile(string filePath);
    }
}