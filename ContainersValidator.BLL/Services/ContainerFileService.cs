﻿using ContainersValidator.Models;
using FluentValidation;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ContainersValidator.BLL.Services
{
    public class ContainerFileService : IContainerFileService
    {
        private AbstractValidator<Container> _containerValidator;

        public ContainerFileService(AbstractValidator<Container> containerValidator)
        {
            _containerValidator = containerValidator;
        }

        public List<ContainerValidationResult> Validate(List<Container> containers)
        {
            var validationResults = new List<ContainerValidationResult>();
            foreach (var container in containers)
            {
                var validationResult = _containerValidator.Validate(container);
                validationResults.Add(new ContainerValidationResult()
                {
                    ContainerNumber = container.Number,
                    Errors = validationResult.Errors.Select(c => c.ErrorMessage).ToList()
                });
            }

            return validationResults;
        }

        public List<Container> ParseFile(string filePath)
        {
            var containers = new List<Container>();
            using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader sr = new StreamReader(file))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null && !string.IsNullOrWhiteSpace(line))
                    {
                        containers.Add(new Container(line));
                    }
                }
            }

            return containers;
        }
    }
}