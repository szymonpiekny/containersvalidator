﻿using ContainersValidator.BLL.CheckDigitCalculators;
using ContainersValidator.Models;
using FluentValidation;
using System.Linq;

namespace ContainersValidator.BLL.Validators
{
    public class ContainerValidator : AbstractValidator<Container>
    {
        private const int REQUIRED_CONTAINER_NUMBER_LENGTH = 11;
        private const int CATEGORY_IDENTIFIER_INDEX = 3;
        private const int SERIAL_NUMBER_INDEX = 4;
        private const int SERIAL_NUMBER_LENGTH = 6;
        private const int OWNER_CODE_LENGTH = 3;
        private const int CHECK_DIGIT_INDEX = 10;
        private static readonly char[] AllowedCategoryIndentifier = { 'U', 'J', 'Z' };

        public ContainerValidator(ICheckDigitCalculator checkDigitCalculator)
        {
            RuleFor(c => c.Number)
                .NotNull().WithMessage($"Container number must consists of {REQUIRED_CONTAINER_NUMBER_LENGTH} characters")
                .Length(REQUIRED_CONTAINER_NUMBER_LENGTH).WithMessage($"Container number must consists of {REQUIRED_CONTAINER_NUMBER_LENGTH} characters")
                .DependentRules(() =>
                {
                    RuleFor(c => c.Number).Must(OwnerCodeContainsOnlyCharacters).WithMessage("Owner code must contains only uppercase letters");
                    RuleFor(c => c.Number).Must(CategoryIdentifierContainsAllowedValue).WithMessage($"Category identifier must be one of: {string.Join(", ", AllowedCategoryIndentifier)}");
                    RuleFor(c => c.Number).Must(SerialNumberContainsOnlyDigits).WithMessage("Serial number must contains only digits");
                    RuleFor(c => c.Number).Must(CheckDigitBeDigit).WithMessage("Check digit must be digit");
                    RuleFor(c => c.Number).Must(c => checkDigitCalculator.IsCheckDigitValid(c)).WithMessage("Check digit is wrong");
                });
        }

        private bool CheckDigitBeDigit(string identifier)
        {
            return char.IsDigit(identifier[CHECK_DIGIT_INDEX]);
        }

        private bool SerialNumberContainsOnlyDigits(string identifier)
        {
            return identifier.Substring(SERIAL_NUMBER_INDEX, SERIAL_NUMBER_LENGTH).All(char.IsDigit);
        }

        private bool CategoryIdentifierContainsAllowedValue(string identifier)
        {
            var categoryIdenifier = identifier[CATEGORY_IDENTIFIER_INDEX];
            return AllowedCategoryIndentifier.Any(c => c == categoryIdenifier);
        }

        private bool OwnerCodeContainsOnlyCharacters(string identifier)
        {
            var ownerCode = identifier.Take(OWNER_CODE_LENGTH);
            foreach (var character in ownerCode)
            {
                if (!IsLatinLetter(character) || !char.IsUpper(character))
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsLatinLetter(char c)
        {
            return c >= 'A' && c <= 'Z';
        }
    }
}