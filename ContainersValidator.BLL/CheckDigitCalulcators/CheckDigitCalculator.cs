﻿using System.Collections.Generic;

namespace ContainersValidator.BLL.CheckDigitCalculators
{
    public class CheckDigitCalculator : ICheckDigitCalculator
    {
        private const int MODULO_DIVISION = 11;
        private const int REQUIRED_CONTAINER_NUMBER_LENGTH = 11;

        private static readonly Dictionary<char, int> _letterValues = new Dictionary<char, int>()
        {
            { 'A', 10 },
            { 'B', 12 },
            { 'C', 13 },
            { 'D', 14 },
            { 'E', 15 },
            { 'F', 16 },
            { 'G', 17 },
            { 'H', 18 },
            { 'I', 19 },
            { 'J', 20 },
            { 'K', 21 },
            { 'L', 23 },
            { 'M', 24 },
            { 'N', 25 },
            { 'O', 26 },
            { 'P', 27 },
            { 'Q', 28 },
            { 'R', 29 },
            { 'S', 30 },
            { 'T', 31 },
            { 'U', 32 },
            { 'V', 34 },
            { 'W', 35 },
            { 'X', 36 },
            { 'Y', 37 },
            { 'Z', 38 }
        };

        private static readonly Dictionary<int, int> _indexMultipliers = new Dictionary<int, int>()
        {
            { 1, 1 },
            { 2, 2 },
            { 3, 4 },
            { 4, 8 },
            { 5, 16 },
            { 6, 32 },
            { 7, 64 },
            { 8, 128 },
            { 9, 256 },
            { 10, 512 }
        };

        public bool IsCheckDigitValid(string identifier)
        {
            if (identifier == null || identifier.Length != REQUIRED_CONTAINER_NUMBER_LENGTH)
            {
                return false;
            }

            var checkDigit = GetCheckDigit(identifier);
            var canParseCheckDigit = int.TryParse(checkDigit.ToString(), out int checkDigitValue);
            if (!canParseCheckDigit)
            {
                return false;
            }

            var sum = CalculateSum(identifier);
            var moduloResult = sum % MODULO_DIVISION;
            return moduloResult == checkDigitValue;
        }

        private char GetCheckDigit(string identifier)
        {
            return identifier[identifier.Length - 1];
        }

        private int CalculateSum(string identifier)
        {
            var calculationPart = GetCalculationPart(identifier);
            int sum = 0;
            var index = 1;
            foreach (var character in calculationPart)
            {
                var characterValue = 0;
                if (char.IsLetter(character))
                {
                    _letterValues.TryGetValue(character, out characterValue);
                }
                else
                {
                    int.TryParse(character.ToString(), out characterValue);
                }

                _indexMultipliers.TryGetValue(index, out int indexMultipier);
                characterValue = characterValue * indexMultipier;
                sum += characterValue;
                index++;
            }

            return sum;
        }

        private string GetCalculationPart(string identifier)
        {
            return identifier.Substring(0, identifier.Length - 1);
        }
    }
}