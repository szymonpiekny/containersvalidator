﻿namespace ContainersValidator.BLL.CheckDigitCalculators
{
    public interface ICheckDigitCalculator
    {
        bool IsCheckDigitValid(string identifier);
    }
}