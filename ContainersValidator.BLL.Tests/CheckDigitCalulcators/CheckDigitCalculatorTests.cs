﻿using ContainersValidator.BLL.CheckDigitCalculators;
using FluentAssertions;
using NUnit.Framework;

namespace ContainersValidator.BLL.Tests.Validators
{
    [TestFixture]
    public class CheckDigitCalculatorTests
    {
        private CheckDigitCalculator _checkDigitCalculator;

        [SetUp]
        public void SetUp()
        {
            _checkDigitCalculator = new CheckDigitCalculator();
        }

        [TestCase("CSQU3054383")]
        [TestCase("TOLU4734787")]
        [TestCase("HJCU8281988")]
        public void IsCheckDigitValid_ProvideProperContainerNumber_ShouldReturnTrue(string containerNumber)
        {
            //Arrange
            //Act
            var result = _checkDigitCalculator.IsCheckDigitValid(containerNumber);

            //Assert
            result.Should().BeTrue();
        }

        [TestCase("CSQU3054381")]
        [TestCase("TOLU4734782")]
        [TestCase("HJCU8281983")]
        [TestCase("HJCU828198H")]
        public void IsCheckDigitValid_ProvideContainerNumberWithWrongCheckDigit_ShouldReturnFalse(string containerNumber)
        {
            //Arrange
            //Act
            var result = _checkDigitCalculator.IsCheckDigitValid(containerNumber);

            //Assert
            result.Should().BeFalse();
        }

        [TestCase("12345")]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("123456789123")]
        public void IsCheckDigitValid_ProvideContainerNumberWithInvalidLenght_ShouldReturnFalse(string containerNumber)
        {
            //Arrange
            //Act
            var result = _checkDigitCalculator.IsCheckDigitValid(containerNumber);

            //Assert
            result.Should().BeFalse();
        }
    }
}