﻿using AutoFixture;
using ContainersValidator.BLL.CheckDigitCalculators;
using ContainersValidator.BLL.Validators;
using ContainersValidator.Models;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace ContainersValidator.BLL.Tests.Validators
{
    [TestFixture]
    public class ContainerNumberValidatorTests
    {
        private ContainerValidator _containerNumberValidator;
        private Fixture _fixture;
        private Mock<ICheckDigitCalculator> _checkDigitCalculator;

        [SetUp]
        public void Setup()
        {
            _fixture = new Fixture();
            _checkDigitCalculator = _fixture.Create<Mock<ICheckDigitCalculator>>();
            _containerNumberValidator = new ContainerValidator(_checkDigitCalculator.Object);
        }

        [TestCase("CSQU3054383")]
        [TestCase("TOLU4734787")]
        [TestCase("HJCU8281988")]
        public void Validate_ProvideValidContainerNumber_ShouldReturnValidStatus(string containerNumber)
        {
            //Arrange
            _checkDigitCalculator.Setup(c => c.IsCheckDigitValid(It.IsAny<string>())).Returns(true);
            var container = new Container(containerNumber);

            //Act
            var result = _containerNumberValidator.Validate(container);

            //Assert
            result.IsValid.Should().BeTrue();
        }

        [TestCase("CSQU3054383")]
        [TestCase("CSQJ3054383")]
        [TestCase("CSQZ3054383")]
        public void Validate_ProvideValidCategoryIdentifier_ShouldReturnInvalidStatus(string containerNumber)
        {
            //Arrange
            _checkDigitCalculator.Setup(c => c.IsCheckDigitValid(It.IsAny<string>())).Returns(true);
            var container = new Container(containerNumber);

            //Act
            var result = _containerNumberValidator.Validate(container);

            //Assert
            result.IsValid.Should().BeTrue();
        }

        [TestCase("123456789123")]
        [TestCase("1234567891")]
        [TestCase("123")]
        [TestCase("")]
        [TestCase(null)]
        public void Validate_ProvideIdenifierWithInvalidLengthContainerNumber_ShouldReturnValidStatus(string containerNumber)
        {
            //Arrange
            _checkDigitCalculator.Setup(c => c.IsCheckDigitValid(It.IsAny<string>())).Returns(true);
            var container = new Container(containerNumber);

            //Act
            var result = _containerNumberValidator.Validate(container);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().ErrorMessage.Should().Be("Container number must consists of 11 characters");
        }

        [TestCase("CSQ@3054383")]
        [TestCase("CSQ13054383")]
        [TestCase("CSQЙ3054383")]
        [TestCase("CSQA3054383")]
        [TestCase("CSQu3054383")]
        [TestCase("CSQj3054383")]
        [TestCase("CSQz3054383")]
        public void Validate_ProvideNotAllowedCategoryIdentifier_ShouldReturnInvalidStatus(string containerNumber)
        {
            //Arrange
            _checkDigitCalculator.Setup(c => c.IsCheckDigitValid(It.IsAny<string>())).Returns(true);
            var container = new Container(containerNumber);

            //Act
            var result = _containerNumberValidator.Validate(container);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().ErrorMessage.Should().Be("Category identifier must be one of: U, J, Z");
        }

        [TestCase("CS@U3054383")]
        [TestCase("C1QU3054383")]
        [TestCase("ЙSQU3054383")]
        [TestCase("cSQU3054383")]
        [TestCase("csqU3054383")]
        public void Validate_ProvideInvalidCharacterInOwnerCode_ShouldReturnInvalidStatus(string containerNumber)
        {
            //Arrange
            _checkDigitCalculator.Setup(c => c.IsCheckDigitValid(It.IsAny<string>())).Returns(true);
            var container = new Container(containerNumber);

            //Act
            var result = _containerNumberValidator.Validate(container);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().ErrorMessage.Should().Be("Owner code must contains only uppercase letters");
        }

        [TestCase("CSQUA054383")]
        [TestCase("CSQUAA54383")]
        [TestCase("CSQUA0A4383")]
        [TestCase("CSQUA05A383")]
        [TestCase("CSQUA054A83")]
        [TestCase("CSQUA0543A3")]
        [TestCase("CSQUAAAAAA3")]
        public void Validate_ProvideInvalidSerialNumber_ShouldReturnInvalidStatus(string containerNumber)
        {
            //Arrange
            _checkDigitCalculator.Setup(c => c.IsCheckDigitValid(It.IsAny<string>())).Returns(true);
            var container = new Container(containerNumber);

            //Act
            var result = _containerNumberValidator.Validate(container);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().ErrorMessage.Should().Be("Serial number must contains only digits");
        }

        [TestCase("CSQU305438@")]
        [TestCase("CSQU305438A")]
        [TestCase("CSQU305438Й")]
        public void Validate_ProvideInvalidCheckDigit_ShouldReturnInvalidStatus(string containerNumber)
        {
            //Arrange
            _checkDigitCalculator.Setup(c => c.IsCheckDigitValid(It.IsAny<string>())).Returns(true);
            var container = new Container(containerNumber);

            //Act
            var result = _containerNumberValidator.Validate(container);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().ErrorMessage.Should().Be("Check digit must be digit");
        }

        [TestCase("1SQU30543812")]
        [TestCase("CSQX30543812")]
        [TestCase("CSQUA0543812")]
        [TestCase("CSQU305438@2")]
        public void Validate_CombinedInvalidPartsWithWrongLength_ShouldReturnErrorAboutLength(string containerNumber)
        {
            //Arrange
            _checkDigitCalculator.Setup(c => c.IsCheckDigitValid(It.IsAny<string>())).Returns(true);
            var container = new Container(containerNumber);

            //Act
            var result = _containerNumberValidator.Validate(container);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().ErrorMessage.Should().Be("Container number must consists of 11 characters");
        }

        [TestCase("1SQX3054312")]
        [TestCase("CSQXA054312")]
        [TestCase("CSQUA05431A")]
        public void Validate_CombinedInvalidParts_ShouldReturnMultipleErrors(string containerNumber)
        {
            //Arrange
            _checkDigitCalculator.Setup(c => c.IsCheckDigitValid(It.IsAny<string>())).Returns(true);
            var container = new Container(containerNumber);

            //Act
            var result = _containerNumberValidator.Validate(container);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(2);
        }

        [Test]
        public void Validate_CheckDigitIsInvalid_ShouldReturnError()
        {
            //Arrange
            _checkDigitCalculator.Setup(c => c.IsCheckDigitValid(It.IsAny<string>())).Returns(false);
            var container = new Container("CSQU3054381");

            //Act
            var result = _containerNumberValidator.Validate(container);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().ErrorMessage.Should().Be("Check digit is wrong");
        }
    }
}