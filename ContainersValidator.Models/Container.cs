﻿namespace ContainersValidator.Models
{
    public class Container
    {
        public Container(string number)
        {
            Number = number;
        }

        public string Number { get; }
    }
}