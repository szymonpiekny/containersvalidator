﻿using System.Collections.Generic;
using System.Linq;

namespace ContainersValidator.Models
{
    public class ContainerValidationResult
    {
        public string ContainerNumber { get; set; }
        public IEnumerable<string> Errors { get; set; }

        public bool IsValid => !Errors.Any();
    }
}